Dies ist das README.md File der API Userverwaltung.

In dieser Applikation können User verwaltet werden. Man kann User abspeichern, ihnen eine Job zuweisen, diesem Job verschiedene Tasks zuweisen 
und auch den Wohnort der User abspeichern. Die Applikation ist zudem mit JWT gesichert, ausser dem Sign-up und dem Login Request, braucht es für 
jeden Request einen dafür erstellten Bearer Token. Diesen erhält man, wenn man sich als User einloggt.

Die Appliaktion wird mit der Klasse "JWT1Application.java" gestartet.

