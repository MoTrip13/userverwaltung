package ch.mbcoding.userManagement;

import ch.mbcoding.userManagement.job.Job;
import ch.mbcoding.userManagement.job.JobRepository;
import ch.mbcoding.userManagement.place.Place;
import ch.mbcoding.userManagement.place.PlaceRepository;
import ch.mbcoding.userManagement.task.Task;
import ch.mbcoding.userManagement.task.TaskRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class Jwt1Application {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CommandLineRunner demoData(TaskRepository taskRepository, JobRepository jobRepository, PlaceRepository placeRepository){
        return (args -> {
            Job j1 = new Job("Entwickelt Appliaktionen", "Applikationsentwickler", 20000);
            Job j2 = new Job("Reinigt Toiletten", "Toilettenreiniger", 2000);

            jobRepository.save(j1);
            jobRepository.save(j2);

            Task t1 = new Task("Muss Programmieren", 1, j1);
            Task t2 = new Task("Muss Lumpen reinigen", 3, j2);

            taskRepository.save(t1);
            taskRepository.save(t2);

            Place p1 = new Place("Winkel", "Schweiz");
            Place p2 = new Place("Winterthur", "Schweiz");

            placeRepository.save(p1);
            placeRepository.save(p2);
        });
    }

    public static void main(String[] args) {
        SpringApplication.run(Jwt1Application.class, args);
    }

}
