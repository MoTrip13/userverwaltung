package ch.mbcoding.userManagement.task;

import ch.mbcoding.userManagement.job.Job;
import com.fasterxml.jackson.annotation.JsonBackReference;


import javax.persistence.*;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private int priority;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id", nullable = true)
    private Job job;

    public Task() {}

    public Task(String description, int priority, Job job) {
        this.priority = priority;
        this.description = description;
        this.job = job;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Task(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}