package ch.mbcoding.userManagement.task;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Task findByDescription(String description);
    Task findByPriority(int priority);
}