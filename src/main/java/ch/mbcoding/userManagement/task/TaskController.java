package ch.mbcoding.userManagement.task;

import ch.mbcoding.userManagement.service.EntityService;
import org.hibernate.persister.walking.spi.EntityIdentifierDefinition;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private EntityService entityService;

    public TaskController(EntityService entityService) {
        this.entityService = entityService;
    }

    @PostMapping("/{id}")
    public void addTask(@RequestBody Task task, @PathVariable long id) {
        entityService.addTask(task, id);
    }

    @GetMapping
    public List<Task> getTasks() {
        return entityService.getTasks();
    }

    @GetMapping("/description/{description}")
    public Task getTaskByDescription(@PathVariable String description) {
        return entityService.getTaskByDescription(description);
    }

    @GetMapping("/priority/{priority}")
    public Task getTaskByPriority(@PathVariable int priority) {
        return entityService.getTaskByPriority(priority);
    }

    @PutMapping("/{id}")
    public void editTask(@PathVariable long id, @RequestBody Task task) {
        entityService.editTask(id, task);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable long id) {
        entityService.deleteTask(id);
    }
}