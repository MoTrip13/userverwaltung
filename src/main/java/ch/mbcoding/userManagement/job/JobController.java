package ch.mbcoding.userManagement.job;

import ch.mbcoding.userManagement.service.EntityService;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:14
 * @project Package ch.mbcoding.userManagement.job von Projekt jwt1
 */

@RestController
@RequestMapping("/jobs")
public class JobController {

    private EntityService entityService;

    public JobController(EntityService entityService) {
        this.entityService = entityService;
    }

    @PostMapping
    public void addJob(@RequestBody Job job) {
        entityService.addJob(job);
    }

    @GetMapping
    public List<Job> getJobs() {
        return entityService.getJobs();
    }

    @GetMapping("/name/{name}")
    public Job getJobByName(@PathVariable String name) {
        return entityService.getJobByName(name);
    }

    @GetMapping("/salary/{salary}")
    public Job getJobBySalary(@PathVariable int salary) {
        return entityService.getJobBySalary(salary);
    }

    @PutMapping("/{id}")
    public void editJob(@PathVariable long id, @RequestBody Job job) {
        entityService.editJob(id, job);
    }

    @DeleteMapping("/{id}")
    public void deleteJob(@PathVariable long id) {
        entityService.deleteJob(id);
    }
}
