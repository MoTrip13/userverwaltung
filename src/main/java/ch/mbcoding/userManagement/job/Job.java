package ch.mbcoding.userManagement.job;

import ch.mbcoding.userManagement.task.Task;
import ch.mbcoding.userManagement.user.ApplicationUser;
import com.fasterxml.jackson.annotation.JsonManagedReference;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:14
 * @project Package ch.mbcoding.userManagement.job von Projekt jwt1
 */
@Entity
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;
    private String name;
    private int salary;

    public Job() {
    }

    public Job(String description, String name, int salary) {
        this.salary = salary;
        this.name = name;
        this.description = description;
    }

    @OneToMany(mappedBy = "job")
    private List<ApplicationUser> users = new ArrayList<ApplicationUser>();

    @JsonManagedReference
    @OneToMany(mappedBy = "job")
    private List<Task> tasks = new ArrayList<Task>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<ApplicationUser> getUsers() {
        return users;
    }

    public void setUsers(List<ApplicationUser> users) {
        this.users = users;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
