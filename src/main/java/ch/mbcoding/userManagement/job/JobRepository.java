package ch.mbcoding.userManagement.job;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:15
 * @project Package ch.mbcoding.userManagement.job von Projekt jwt1
 */
public interface JobRepository extends JpaRepository<Job, Long> {
    Job findByName(String name);
    Job findBySalary(int salary);
}
