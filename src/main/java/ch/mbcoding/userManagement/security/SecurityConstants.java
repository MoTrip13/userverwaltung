package ch.mbcoding.userManagement.security;

/**
 * @author Moritz Bolliger
 * @version 08.07.2020 13:28
 * @project Package ch.mbcoding.jwt1.security von Projekt jwt1
 */
public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
}
