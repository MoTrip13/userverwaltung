package ch.mbcoding.userManagement.place;

import ch.mbcoding.userManagement.user.ApplicationUser;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:16
 * @project Package ch.mbcoding.userManagement.place von Projekt jwt1
 */
@Entity
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String city;
    private String country;

    @OneToMany(mappedBy = "place")
    private List<ApplicationUser> users = new ArrayList<ApplicationUser>();

    public Place() {
    }

    public Place(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<ApplicationUser> getUsers() {
        return users;
    }

    public void setUsers(List<ApplicationUser> users) {
        this.users = users;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
