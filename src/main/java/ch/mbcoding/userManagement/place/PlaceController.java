package ch.mbcoding.userManagement.place;

import ch.mbcoding.userManagement.job.Job;
import ch.mbcoding.userManagement.job.JobRepository;
import ch.mbcoding.userManagement.service.EntityService;
import org.springframework.security.core.parameters.P;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:16
 * @project Package ch.mbcoding.userManagement.place von Projekt jwt1
 */

@RestController
@RequestMapping("/places")
public class PlaceController {

    private EntityService entityService;

    public PlaceController(EntityService entityService) {
        this.entityService = entityService;
    }

    @PostMapping
    public void addPlace(@RequestBody Place place) {
        entityService.addPlace(place);
    }

    @GetMapping
    public List<Place> getPlaces() {
        return entityService.getPlaces();
    }

    @GetMapping("/city/{city}")
    public Place getPlaceByCity(@PathVariable String city) {
        return entityService.getPlaceByCity(city);
    }

    @GetMapping("/country/{country}")
    public Place getPlaceByCountry(@PathVariable String country) {
        return entityService.getPlaceByCountry(country);
    }

    @PutMapping("/{id}")
    public void editPlace(@PathVariable long id, @RequestBody Place place) {
        entityService.editPlace(id, place);
    }

    @DeleteMapping("/{id}")
    public void deletePlace(@PathVariable long id) {
        entityService.deletePlace(id);
    }
}
