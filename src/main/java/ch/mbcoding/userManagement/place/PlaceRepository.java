package ch.mbcoding.userManagement.place;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:16
 * @project Package ch.mbcoding.userManagement.place von Projekt jwt1
 */
public interface PlaceRepository extends JpaRepository<Place, Long> {
    Place findByCity(String city);
    Place findByCountry(String country);
}
