package ch.mbcoding.userManagement.user;

/**
 * @author Moritz Bolliger
 * @version 08.07.2020 12:57
 * @project Package com.auth0.samples.authapi.springbootauthupdated.user von Projekt springboot-auth-updated
 */
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
    ApplicationUser findByFirstname(String firstname);
    ApplicationUser findByLastname(String lastname);
}
