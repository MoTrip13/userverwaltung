package ch.mbcoding.userManagement.user;

/**
 * @author Moritz Bolliger
 * @version 08.07.2020 12:58
 * @project Package com.auth0.samples.authapi.springbootauthupdated.user von Projekt springboot-auth-updated
 */
import ch.mbcoding.userManagement.place.Place;
import ch.mbcoding.userManagement.service.EntityService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private EntityService entityService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(EntityService entityService,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.entityService = entityService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) {
        entityService.signUp(user);
    }

    @GetMapping
    public List<ApplicationUser> getUsers() {
        return entityService.getUsers();
    }

    @GetMapping("/firstname/{firstname}")
    public ApplicationUser getUserByFirstname(@PathVariable String firstname) {
        return entityService.getUserByFirstname(firstname);
    }

    @GetMapping("/lastname/{lastname}")
    public ApplicationUser getUserByLastname(@PathVariable String lastname) {
        return entityService.getUserByLastname(lastname);
    }

    @GetMapping("/username/{username}")
    public ApplicationUser getUserByUsername(@PathVariable String username) {
        return entityService.getUserByUsername(username);
    }

    @PutMapping("/{id}")
    public void editUser(@PathVariable long id, @RequestBody ApplicationUser user) {
        entityService.editUser(id, user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable long id) {
        entityService.deleteUser(id);
    }
}
