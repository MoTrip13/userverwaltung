package ch.mbcoding.userManagement.user;

/**
 * @author Moritz Bolliger
 * @version 08.07.2020 12:55
 * @project Package com.auth0.samples.authapi.springbootauthupdated.user von Projekt springboot-auth-updated
 */
import ch.mbcoding.userManagement.job.Job;
import ch.mbcoding.userManagement.place.Place;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;


    @ManyToOne
    @JoinColumn(name = "place_id", referencedColumnName = "id", nullable = true)
    private Place place;


    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id", nullable = true)
    private Job job;

    public ApplicationUser() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
