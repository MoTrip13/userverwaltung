package ch.mbcoding.userManagement.service;

import ch.mbcoding.userManagement.job.Job;
import ch.mbcoding.userManagement.job.JobRepository;
import ch.mbcoding.userManagement.place.Place;
import ch.mbcoding.userManagement.place.PlaceRepository;
import ch.mbcoding.userManagement.task.Task;
import ch.mbcoding.userManagement.task.TaskRepository;
import ch.mbcoding.userManagement.user.ApplicationUser;
import ch.mbcoding.userManagement.user.ApplicationUserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Moritz Bolliger
 * @version 10.07.2020 08:16
 * @project Package ch.mbcoding.userManagement.service von Projekt jwt1
 */
@RestController
public class EntityService {
    TaskRepository taskRepository;
    ApplicationUserRepository applicationUserRepository;
    PlaceRepository placeRepository;
    JobRepository jobRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public EntityService(TaskRepository taskRepository, ApplicationUserRepository applicationUserRepository, PlaceRepository placeRepository, JobRepository jobRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.taskRepository = taskRepository;
        this.applicationUserRepository = applicationUserRepository;
        this.placeRepository = placeRepository;
        this.jobRepository = jobRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void addTask(Task task, long id) {
        Job job = jobRepository.findById(id).get();
        task.setJob(job);
        taskRepository.save(task);
    }

    public List<Task> getTasks() {
        return taskRepository.findAll();
    }

    public void editTask( long id,  Task task) {
        Task existingTask = taskRepository.findById(id).get();
        Assert.notNull(existingTask, "Task not found");
        existingTask.setDescription(task.getDescription());
        existingTask.setPriority(task.getPriority());
        taskRepository.save(existingTask);
    }

    public void deleteTask( long id) {
        Task taskToDel = taskRepository.findById(id).get();
        taskRepository.delete(taskToDel);
    }

    public void addJob( Job job) {
        jobRepository.save(job);
    }

    public List<Job> getJobs() {
        return jobRepository.findAll();
    }

    public void editJob( long id,  Job job) {
        Job existingJob = jobRepository.findById(id).get();
        Assert.notNull(existingJob, "Task not found");
        existingJob.setDescription(job.getDescription());
        existingJob.setName(job.getName());
        existingJob.setSalary(job.getSalary());
        jobRepository.save(existingJob);
    }

    public void deleteJob( long id) {
        Job jobToDel = jobRepository.findById(id).get();
        jobRepository.delete(jobToDel);
    }

    public void addPlace( Place place) {
        placeRepository.save(place);
    }

    public List<Place> getPlaces() {
        return placeRepository.findAll();
    }

    public void editPlace( long id,  Place place) {
        Place existingPlace = placeRepository.findById(id).get();
        Assert.notNull(existingPlace, "Task not found");
        existingPlace.setCity(place.getCity());
        existingPlace.setCountry(place.getCountry());
        placeRepository.save(existingPlace);
    }

    public void deletePlace( long id) {
        Place placeToDel = placeRepository.findById(id).get();
        placeRepository.delete(placeToDel);
    }

    public void signUp(ApplicationUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
    }

    public List<ApplicationUser> getUsers() {
        return applicationUserRepository.findAll();
    }

    public void editUser( long id,  ApplicationUser user) {
        ApplicationUser existingUser = applicationUserRepository.findById(id).get();
        Assert.notNull(existingUser, "Task not found");
        existingUser.setPassword(user.getPassword());
        existingUser.setUsername(user.getUsername());
        existingUser.setFirstname(user.getFirstname());
        existingUser.setLastname(user.getLastname());
        applicationUserRepository.save(existingUser);
    }

    public void deleteUser( long id) {
        ApplicationUser userToDel = applicationUserRepository.findById(id).get();
        applicationUserRepository.delete(userToDel);
    }

    public ApplicationUser getUserByFirstname( String firstname) {
        return applicationUserRepository.findByFirstname(firstname);
    }

    public ApplicationUser getUserByLastname( String lastname) {
        return applicationUserRepository.findByLastname(lastname);
    }

    public ApplicationUser getUserByUsername( String username) {
        return applicationUserRepository.findByUsername(username);
    }

    public Task getTaskByDescription(String description) {
        return taskRepository.findByDescription(description);
    }

    public Task getTaskByPriority(int priority) {
        return taskRepository.findByPriority(priority);
    }

    public Place getPlaceByCity(String city) {
        return placeRepository.findByCity(city);
    }

    public Place getPlaceByCountry(String country) {
        return placeRepository.findByCountry(country);
    }

    public Job getJobByName(String name) {
        return jobRepository.findByName(name);
    }

    public Job getJobBySalary(int salary) {
        return jobRepository.findBySalary(salary);
    }
}
